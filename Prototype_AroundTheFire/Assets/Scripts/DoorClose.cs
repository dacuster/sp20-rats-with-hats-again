﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorClose : MonoBehaviour
{
    public float angleThreshold = 10f;

    private HingeJoint joint;
    private Vector3 originalAngles;
    [SerializeField] private DoorGrabbable grabHandler;

    void Start()
    {
        joint = GetComponent<HingeJoint>();
        originalAngles = transform.eulerAngles;
    }

    void Update()
    {
        if(!grabHandler.isGrabbed && joint.angle <= angleThreshold && joint.angle >= -angleThreshold)
        {
            transform.eulerAngles = originalAngles;
        }
    }
}
