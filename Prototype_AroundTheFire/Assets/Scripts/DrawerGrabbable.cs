﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerGrabbable : OVRGrabbable
{
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;
    [SerializeField] private float springSnapOffset = 0.05f;

    [Space]

    public bool m_disableObjectRotation = false;

    public bool m_restrictXMovement = false;

    public bool m_restrictYMovement = false;

    public bool m_restrictZMovement = false;

    [SerializeField] private SpringJoint joint;
    

    protected override void Start()
    {
        m_grabbedKinematic = GetComponent<Rigidbody>().isKinematic;

        if (joint == null)
            joint = GetComponent<SpringJoint>();

        joint.maxDistance = (end.position - start.position).magnitude;

        transform.position = start.position;
    }

    void Update()
    {
        if((transform.position - start.position).magnitude <= springSnapOffset)
        {
            transform.position = start.position;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        //fixes infinite pull bug
        Vector3 direction = transform.position - end.position;
        float angle = Vector3.Angle(transform.forward, direction);
        if(isGrabbed && Mathf.Abs(angle) <= 90f)
        {
            grabbedBy.ForceRelease(this);
        }

        //fixes backwards push bug
        direction = start.position - transform.position;
        angle = Vector3.Angle(transform.forward, direction);
        if(Mathf.Abs(angle) <= 90f)
        {
            transform.position = start.position;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
