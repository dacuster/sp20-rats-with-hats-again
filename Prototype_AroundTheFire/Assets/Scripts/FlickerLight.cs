﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerLight : MonoBehaviour
{
    [SerializeField] private Light light;
    [SerializeField] private Texture2D colorSample;

    [SerializeField] private float speedModifier = 1.0f;
    [SerializeField] private float upperSpeedRange;
    [Min(0)] [SerializeField] private float lowerSpeedRange;
    [SerializeField] private float switchTimer = 0.25f;

    private float randomSpeed = 1.0f;
    [SerializeField] private Color currentColor, nextColor;
    private float timer = 0.0f;

    void Start()
    {
        light = GetComponent<Light>();
        currentColor = light.color;
        nextColor = GetSampleColor();
    }

    void Update()
    {
        timer += Time.deltaTime;

        if(timer >= switchTimer || light.color.Equals(nextColor))
        {
            nextColor = GetSampleColor();
            randomSpeed = Random.Range(lowerSpeedRange, upperSpeedRange) * speedModifier;

            timer = 0.0f;
        }

        light.color = Color.Lerp(currentColor, nextColor, randomSpeed * Time.deltaTime);
        currentColor = light.color;
    }

    Color GetSampleColor()
    {
        int randX = Random.Range(0, colorSample.width - 1);
        int randY = Random.Range(0, colorSample.height - 1);

        return colorSample.GetPixel(randX, randY);
    }
}
