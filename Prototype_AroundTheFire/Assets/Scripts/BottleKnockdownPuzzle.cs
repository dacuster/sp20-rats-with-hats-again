﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleKnockdownPuzzle : MonoBehaviour
{
    [SerializeField] GameObject potion_blue;
    [SerializeField] Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //If another puzzle object is thrown at this bottle, set to grabbable layer and launch away from wall.
        if (other.transform.CompareTag("PuzzleObject"))
        {
            //Checks for any other puzzle object, since Potion_Blue is always inside this trigger.
            if (other.transform.GetComponent<PuzzleObject>().objectID != "Potion_Blue")
            {
                potion_blue.layer = 8;

                //Trigger bottle launch animation.
                animator.SetTrigger("Knockdown");

                //potion_blue.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -10f));
            }
        }
    }

    public void DisableAnim()
    {
        GetComponent<Animator>().enabled = false;
    }
}
